const http = require('http');

const PORT = process.env.PORT || 5000;
const HOST = '0.0.0.0';

const message = 'Hello Google App Engine =-] :-)';

const app_server = http.createServer((req, res) => res.end(message));

app_server.listen(PORT, HOST, (err) => {
    if (err) console.error('Error', err);
    else console.log('Listening for requests');
});
